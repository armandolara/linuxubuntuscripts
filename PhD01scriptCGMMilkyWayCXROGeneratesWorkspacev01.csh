# Main location ok_ files
set path01=`pwd`

# Versions
set AAversion = v01	# General
set ABversion = v01	# Telescopes
set ACversion = v01	# Cameras
set ADversion = v01	# Classes
set AEversion = v01	# Objects
set AFversion = v01	# ObsIDs

# Project PhD01
set project = `sed -n 1p PhD01listCGMMilkyWayKeyNameProject${AAversion}.txt  | awk '{print $1}'`

# Subject Milky Way
set subject = `sed -n 1p PhD01listCGMMilkyWayStudySubjects${AAversion}.txt  | awk '{print $1}'`

# Loop over the study categories
set number_of_categories = `wc PhD01list${subject}StudyCategories${AAversion}.txt` 
@ index_of_category = 1
while ($index_of_category <= $number_of_categories[1])
############################################################################################################################
set category = `sed -n ${index_of_category}p PhD01list${subject}StudyCategories${AAversion}.txt  | awk '{print $1}'`

# Loop over the particular study categories
set number_of_partcategories = `wc PhD01list${subject}StudyCategories${category}${AAversion}.txt` 
@ index_of_partcategory = 1
while ($index_of_partcategory <= $number_of_partcategories[1])
############################################################################################################################
set partcategory = `sed -n ${index_of_partcategory}p PhD01list${subject}StudyCategories${category}${AAversion}.txt  | awk '{print $1}'`

# Telescope
set telescope = `sed -n 2p PhD01list${subject}Telescopes${ABversion}.txt  | awk '{print $1}'`

# Loop over the cameras
set number_of_cameras = `wc PhD01list${subject}${telescope}Cameras${ACversion}.txt` 
@ index_of_camera = 1
while ($index_of_camera <= $number_of_cameras[1])
############################################################################################################################
set camera = `sed -n ${index_of_camera}p PhD01list${subject}${telescope}Cameras${ACversion}.txt  | awk '{print $1}'`

# Loop over the classs
set number_of_classs = `wc PhD01list${subject}Classes${ADversion}.txt` 
@ index_of_class = 1
while ($index_of_class <= $number_of_classs[1])
############################################################################################################################
set class = `sed -n ${index_of_class}p PhD01list${subject}Classes${ADversion}.txt  | awk '{print $1}'`

# Loop over the objects
set number_of_objects = `wc PhD01list${subject}${telescope}Objcts${class}${AEversion}.txt` 
@ index_of_object = 1
while ($index_of_object <= $number_of_objects[1])
############################################################################################################################
set object = `sed -n ${index_of_object}p PhD01list${subject}${telescope}Objcts${class}${AEversion}.txt  | awk '{print $1}'`

# Loop over the obsids
set number_of_obsids = `wc PhD01list${subject}${telescope}${class}ObsIDs${object}${AFversion}.txt` 
@ index_of_obsid = 1
while ($index_of_obsid <= $number_of_obsids[1])
############################################################################################################################
set obsid = `sed -n ${index_of_obsid}p PhD01list${subject}${telescope}${class}ObsIDs${object}${AFversion}.txt  | awk '{print $1}'`

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

# Generates Dta / (DonwExtrct and RepRed) Directories
if ("$category" == "Dta" && "$partcategory" == "DownExtrct" || "$category" == "Dta" && "$partcategory" == "ReprocReduc") then
	mkdir -p {${project}${subject}${category}/{${project}${subject}${category}${partcategory}/{${project}${subject}${category}${partcategory}${telescope}${class}/{${project}${subject}${category}${partcategory}${telescope}${class}${object}}}}}
	
		
# Generates Dta / SelecComb 
else if ("$category" == "Dta" && "$partcategory" == "SelectComb") then
	mkdir -p {${project}${subject}${category}/{${project}${subject}${category}${partcategory}/{${project}${subject}${category}${partcategory}${telescope}${camera}${class}/{${project}${subject}${category}${partcategory}${telescope}${camera}${class}${object}/{${project}${subject}${category}${partcategory}${telescope}${camera}${class}${object}ObsID${obsid},${project}${subject}${category}${partcategory}${telescope}${camera}${class}${object}ObsIDAll},${project}${subject}${category}${partcategory}${telescope}${camera}${class}AllObjcts}}}}

	
 # Generates Anlys / General Directories
else if ("$category" == "Anlys" && "$partcategory" == "General") then
	mkdir -p {${project}${subject}${category}/{${project}${subject}${category}${partcategory}/{${project}${subject}${category}${partcategory}${telescope}${camera}${class}/{${project}${subject}${category}${partcategory}${telescope}${camera}${class}${object}/{${project}${subject}${category}${partcategory}${telescope}${camera}${class}${object}ObsID${obsid},${project}${subject}${category}${partcategory}${telescope}${camera}${class}${object}ObsIDAll},${project}${subject}${category}${partcategory}${telescope}${camera}${class}AllObjcts}}}}
	


# Generates Anlys / (Gauss, HPhaseNoMetal, HPhase, PHase) And Rslts / (Gauss, HPhaseNoMetal, HPhase, PHase) diriectories 
else
	mkdir -p {${project}${subject}${category}/{${project}${subject}${category}${partcategory}/{${project}${subject}${category}${partcategory}${telescope}${camera}${class}/{${project}${subject}${category}${partcategory}${telescope}${camera}${class}${object},${project}${subject}${category}${partcategory}${telescope}${camera}${class}AllObjcts}}}}
endif

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

@ index_of_obsid ++
############################################################################################################################
end

@ index_of_object ++
############################################################################################################################
end

@ index_of_class ++
############################################################################################################################
end

@ index_of_camera ++
############################################################################################################################
end

@ index_of_partcategory ++
############################################################################################################################
end

@ index_of_category ++
############################################################################################################################
end